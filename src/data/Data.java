package data;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Vidmantas Naravas on 10/26/2016.
 *
 * This class was specifically designed to work with breast cancer data set.
 * Although, It was made as versatile as possible, It may need to change some parameters to adopt it to work with different data sets.
 * First
 */

public class Data {

    // Number of training and testing instances aka rows in the data set
    private int numberOfTrainingInstances;
    private int numberOfTestInstances;
    // holds actual data from the set in string format
    private String[][] trainData, testData, tempArrayForWriter;
    // normalized data set is stored here
    private Double[][] normTrainData, normTestData;
    //num of rows and column in the data set
    private int dataR, dataC;

    /**
     * Constructor
     *
     * @param numRow number of rows
     * @param numCol number of columns
     */
    public Data(int numRow, int numCol) {
        dataR = numRow;
        dataC = numCol;
        trainData = new String[numRow][numCol];
        testData = new String[numRow][numCol];
        tempArrayForWriter = new String[numRow][numCol + 1];
        normTrainData = new Double[numRow][numCol];
        normTestData = new Double[numRow][numCol];

    }
    /**
     * Add train/test data to the program.
     *
     * @param trainDataName path to the data set file. If the file is in the project folder, just name of the file is fine.
     * @param data choose whether is the training data or testing data, i.e: "test" or "train"
     * @throws IOException if the file is not found.
     */
    public void addData(String trainDataName, String data) throws IOException {
        //this is used to read from CSV file
        CSVReader reader = new CSVReader(new FileReader(trainDataName));
        String[] nextLine;
        //while non empty line exist do
        int trainD = 0;
        int testD = 0;
        // if its train data, add it to the train data set
        if (data.equalsIgnoreCase("train")) {
            while ((nextLine = reader.readNext()) != null) {
                for (int j = 0; j < dataC; j++)
                    trainData[trainD][j] = nextLine[j];
                trainD++;
            }
            // number of rows (instances) in the train data set
            numberOfTrainingInstances = trainD;
            // normalized data set
            normTrainData = normalizeData(trainData, numberOfTrainingInstances);
        }
        // if its test data, add it to the test data set
        if (data.equalsIgnoreCase("test")) {
            while ((nextLine = reader.readNext()) != null) {
                for (int j = 0; j < dataC; j++)
                    testData[testD][j] = nextLine[j];
                testD++;
            }
            numberOfTestInstances = testD;
            normTestData = normalizeData(testData, numberOfTestInstances);
            fillWriterArray(testData);
        }
    }

    /**
     * Normalizes attribute values using standard deviation.
     *
     * Steps involved:
     * 1) Calculate mean of the column.
     * 2) Subtract the mean from all the values and square each value.
     * 3) Deviation is square root of the total sum of the values divided by the number of values in the column.
     *      i.e. add the values from step 2. Get one total sum. Divide it from total number of attributes. Take square value after division.
     * 4) We get normalized value by subtracting the mean from each value and divide it by their standard deviation.
     *      i.e originalX = (originalX - Mean) / Deviation
     *
     *
     * @param dataIn
     * @return double normalized array by their standard deviation
     */
    public Double[][] normalizeData(String[][] dataIn, int instances) {
        // convert data set to double data set
        Double[][] tempData = convertData(dataIn, instances);
        ArrayList<Double> tempDev = new ArrayList<>();


        double mean;
        double sum = 0.0;
        double deviation;
        int column = 1;
        // While loop for every column but last
        while (column < dataC - 1) {

            //summing column values
            for (int row = 1; row < instances; row++) {
                for (int col = column; col < column + 1; col++) {
                    sum = sum + tempData[row][col];
                }
            }

            //calculate mean of the column values
            mean = sum / (instances-1);


            // calculate
            for (int row = 1; row < instances; row++) {
                for (int col = column; col < column + 1; col++) {
                    tempDev.add(Math.pow(tempData[row][col] - mean, 2));
                }
            }
            // resetting sum value to 0
            sum = 0.0;
            //summing values
            for (Double d : tempDev)
                sum += d;


            // Calculating deviation
            deviation = Math.sqrt(sum / (double)instances);


            for (int row = 1; row < instances; row++) {
                for (int col = column; col < column + 1; col++) {
                    tempData[row][col] = (tempData[row][col] - mean) / deviation;
                }
            }
            // resetting values after each column
            mean = 0.0;
            sum = 0.0;
            deviation = 0.0;
            tempDev.clear();
            column++; // increase column
        }
        return tempData;
    }


    /**
     * Converts data for String to double
     *
     * @param dataIn
     * @return double array
     */
    public Double[][] convertData(String[][] dataIn, int instances) {
        Double[][] tempData = new Double[dataIn.length][dataIn.length];
        // convert to double  2d array
        for (int row = 1; row < instances; row++) {
            for (int col = 1; col < dataIn[row].length - 1; col++) {
                tempData[row][col] = Double.valueOf(dataIn[row][col]);
            }
        }
        return tempData;
    }


    /**
     * Writes csv file on the disk.
     *
     * @param fileName name of the file to be saved.
     * @throws IOException
     */
    public void saveCSV(String fileName) throws IOException {


        CSVWriter writer = new CSVWriter(new FileWriter(fileName));

        for (int row = 0; row < numberOfTestInstances; row++) {

            int columnCount = tempArrayForWriter[row].length;
            String[] values = new String[columnCount];

            for (int col = 0; col < columnCount; col++) {
                values[col] = tempArrayForWriter[row][col] + ",";
                if (col == tempArrayForWriter[row].length - 1) {
                    values[col] = tempArrayForWriter[row][col];
                }
            }
            writer.writeNext(values);
        }
        System.out.println("File " + fileName + " has been successfully saved at your working directory.");
        writer.flush();
        writer.close();


    }

    /**
     * Method adds another column to the array for the output file
     *
     * @param arrayIn
     * @return array for the output file
     */
    public String[][] fillWriterArray(String[][] arrayIn) {

        for (int row = 0; row < numberOfTestInstances; row++) {
            for (int col = 0; col < tempArrayForWriter[row].length; col++) {

                if (row == 0) {
                    if (col == arrayIn[row].length) {
                        break;
                    }
                    tempArrayForWriter[row][tempArrayForWriter[row].length - 1] = "PREDICTIONS";
                }


                if (col == tempArrayForWriter[row].length - 1 && row >= 1) {
                    if (tempArrayForWriter[row][tempArrayForWriter[row].length - 1] == null) {
                        tempArrayForWriter[row][tempArrayForWriter[row].length - 1] = "...";
                        break;
                    }
                }
                tempArrayForWriter[row][col] = arrayIn[row][col];

            }
        }
        return tempArrayForWriter;
    }

    /**
     * Returns temporary array for csv writer to write on the file later
     *
     * @return tempArrayForWriter
     */
    public String[][] getTempArrayForWriter() {
        return tempArrayForWriter;
    }

    /**
     * Set this array for storing in the file.
     *
     * @param tempArrayForWriter
     */
    public void setTempArrayForWriter(String[][] tempArrayForWriter) {
        this.tempArrayForWriter = tempArrayForWriter;
    }

    /**
     * Returns number of test instances
     *
     * @return numberOfTestInstances
     */
    public int getNumberOfTestInstances() {
        return numberOfTestInstances;
    }

    /**
     * Returns normalized training data set
     *
     * @return normTrainData
     */
    public Double[][] getNormTrainData() {
        return normTrainData;
    }

    /**
     * Returns normalized testing data set
     *
     * @return normTestData
     */
    public Double[][] getNormTestData() {
        return normTestData;
    }

    /**
     * Returns training data set
     *
     * @return train data set
     */
    public String[][] getTrainData() {
        return trainData;
    }

    /**
     * Returns test data set
     *
     * @return testData
     */
    public String[][] getTestData() {
        return testData;
    }

    /**
     * Returns number of training instances (rows in the data set)
     *
     * @return numberOfTrainingInstances
     */
    public int getNumberOfTrainingInstances() {
        return numberOfTrainingInstances;
    }

    /**
     * @return array size for rows
     */
    public int getDataR() {
        return dataR;
    }

    /**
     * @return array size for columns
     */
    public int getDataC() {
        return dataC;
    }

}

