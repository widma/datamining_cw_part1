package kNN;

import data.Data;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vidmantas Naravas on 10/26/2016.
 *
 * Class implements kNN algorithm.
 * For this implementation, Euclidean distance was used.
 *
 * Before running this class, data set class needs to be initialised with correct data sets.
 */
public class kNN_classifier {

    Double[] newInstance;//this array will contain a new instance / new patient symptoms
    Double[] distance;   //this array will contain the distance from the new instance to each instance in the data set
    Data data; // data object

    // variables for the confusion matrix
    int correct, incorrect;
    double trueB, trueM, falseB, falseM = 0.0;

    /**
     * Constructs classifier class with data object
     *
     * @param dataObjectIn object of type Data
     */
    public kNN_classifier(Data dataObjectIn) throws IOException {

        data = dataObjectIn;
        newInstance = new Double[data.getDataC()];
        distance = new Double[data.getDataR()];
        correct = 0;
        incorrect = 0;
    }

    /**
     * k nearest neighbour method.
     * Takes value "k" for the number of nearest neighbours.
     * Calculates distances for each test instance to every instance in the data set.
     * Sorts them by distance, takes smallest k distances.
     * Checks whether it guessed correctly or no, gets values for confusion matrix.
     * Prints confusion matrix.
     *
     * @param k k value for the number of the nearest neighbours
     * @throws IOException
     */
    public void kNN(int k) throws IOException {
        // Little check to termiante the program, if chosen k (neighbours) exceeds number of training instances.
        // Otherwise it would throw Array out of bound exception and terminates the program.
        if (k > data.getNumberOfTrainingInstances() - 1) {
            System.out.println();
            System.out.println("Chosen k is too big. Please choose smaller one. ");
            System.out.println();
            return;
        }
        // class variables
        ArrayList<Instances> predDiagn = new ArrayList<>();
        Double[][] tempTest = data.getNormTestData();
        Double[][] temptTrain = data.getNormTrainData();
        String[][] tempStringTrain = data.getTrainData();
        String[][] tempStringTest = data.getTestData();
        String[][] tempWriterArr = data.getTempArrayForWriter();
        double first, second;
        int i, j = 0;
        int rowIncrease = 1;

        // while rowIncrease is not exceeding number of rows in test data DO
        while (rowIncrease < data.getNumberOfTestInstances()) {
            // getting row from the test set
            for (int row = 1; row < rowIncrease + 1; row++) {
                for (int col = 1; col < data.getDataC(); col++) {
                    newInstance[col] = tempTest[row][col];
                }
            }
            //calculate Euclidean  distance to each of the neighbours
            for (i = 1; i <= data.getNumberOfTrainingInstances() -1; i++) {
                double tempD = 0.0;
                for (j = 1; j < data.getDataC() - 1; j++) {
                    first = newInstance[j]; //test data
                    second = temptTrain[i][j]; //train data
                    tempD += Math.pow(first - second, 2);
                }
                //add distance to the data set
                distance[i] = Math.sqrt(tempD);
                //Adding distance and and their corresponding diagnose from training data set to the class.
                predDiagn.add(new Instances(tempStringTrain[i][j], distance[i]));
            }

            // sorting shortest to longest distances using nw lambda notation
            /*
             * NOTE this lambda notation only works from Java8 if this doesn't work, please comment
             * it out, and uncomment one below.
             */
            // This notation
            Collections.sort(predDiagn, (p1, p2) -> Double.compare(p1.getDistance(), p2.getDistance()));


            /*
              // For java versions below 8
           // sorting shortest to longest distances
            Collections.sort(predDiagn, new Comparator<Instances>() {
                @Override

                public int compare(Instances p1, Instances p2) {
                    return Double.compare(p1.getDistance(), p2.getDistance());
                }

            });
*/
            // this loop loops through shortest diagnoses given by k,
            // adds each to them a weight
            // checks if diagnose is already in the map, if it is, just update a key,
            // if not, add it to the map.
            Map<String, Double> tempD = new HashMap<>();
            for (int diagnoses = 0; diagnoses < k; diagnoses++) {
                double weight = 1 / (1 + predDiagn.get(diagnoses).getDistance());
                String diagnose = predDiagn.get(diagnoses).getDiagnose();
                double distance = tempD.containsKey(diagnose) ? tempD.get(diagnose) : 0;
                distance += weight;
                tempD.put(diagnose, distance);
            }

            // Getting max value out of the hash map:
            // reference: http://stackoverflow.com/questions/5911174/finding-key-associated-with-max-value-in-a-java-map
            Map.Entry<String, Double> maxEntry = null;
            for (Map.Entry<String, Double> entry : tempD.entrySet()) {
                if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                    maxEntry = entry;
                }
            }

            // These if/else statements are for determining if prediction was correct
            // all the values being updated here is used for confusion matrix.
            if (maxEntry.getKey().equalsIgnoreCase(tempStringTest[rowIncrease][j])) {
                if (tempStringTest[rowIncrease][j].equalsIgnoreCase("benign")) {
                    trueB += 1.0;
                }
                if (tempStringTest[rowIncrease][j].equalsIgnoreCase("malign")) {
                    trueM += 1.0;
                }
                correct++;
            } else {
                if (maxEntry.getKey().equalsIgnoreCase("benign") && tempStringTest[rowIncrease][j].equalsIgnoreCase("malign")) {
                    falseB += 1.0;
                }
                if (maxEntry.getKey().equalsIgnoreCase("malign") && tempStringTest[rowIncrease][j].equalsIgnoreCase("benign")) {
                    falseM += 1.0;
                }
                incorrect++;
            }
            //adds predicted diagnose to the output array.
            tempWriterArr[rowIncrease][j + 1] = maxEntry.getKey();
            rowIncrease++; // increase row by 1
            // resetting all arrays for new instance
            predDiagn.clear();
            newInstance = new Double[data.getDataC()]; //this array will contain a new instance / new patient symptoms
            distance = new Double[data.getDataR()]; //this array will contain the distance from the new instance to each instance in the dataset
        }

        // Print number of correctly and incorrectly classified
        System.out.println("Correctly classified: " + correct);
        System.out.println("Incorrectly classified: " + incorrect);
        System.out.println();
        // Confusion matrix
        printConfMatrix(k);
        System.out.println();
        // add each predicted diagnose to the output array
        data.setTempArrayForWriter(tempWriterArr);
        //call method to save csm file onto disk.
        data.saveCSV("predictions" + k + ".csv");
        System.out.println();
    }

    /**
     * This method prints most simple confusion matrix.
     *
     * @param kUsed k neareast neighbours
     */
    public void printConfMatrix(int kUsed) {

        /*
            if it was classified as benign - benign  = true accept class benign
            if it was classified as malign - malign = true accept for the class malign
            if it was classified as benign, but actually is malign = false accept for benign
            if it was classified as malign, but actually is benign = false accept as malign

            *******************************
            *         Malign      Benign  *
            * Malign     TM          FB   *
            * Benign     FM          TB   *
            *******************************

         */

        // performance measures with respect  to the class "malign"
        double acc, sensi, speci, prec = 0.0;

        acc = (trueB + trueM) / (trueB + trueM + falseB + falseM);
        sensi = trueM / (trueM + falseM);
        prec = trueM / (trueM + falseM);
        speci = trueB / (trueB + falseB);
        System.out.println();
        System.out.println("   when k value is: " + kUsed);
        System.out.println("          Predicted ");
        System.out.println("      Malign" + "    Benign");
        System.out.println("Malign " + (int) trueM + "      " + (int) falseB);
        System.out.println("Benign " + (int) falseM + "        " + (int) trueB);
        System.out.println();


        System.out.println("Accuracy: " + new DecimalFormat("#.##%").format(acc));
        System.out.println("Sensitivity: " + new DecimalFormat("#.##%").format(sensi));
        System.out.println("Precision: " + new DecimalFormat("#.##%").format(prec));
        System.out.println("Specificity: " + new DecimalFormat("#.##%").format(speci));
    }
}
