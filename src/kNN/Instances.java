package kNN;

/**
 * Created by Vidmantas Naravas on 10/26/2016.
 *
 * This class stores every training instance with distances from
 * testing instance with their respecting diagnose and distance.
 *
 */
public class Instances {
    private String diagnose;
    private double distance;
    /**
     * @param inDiagnose diagnose
     * @param newDistance distance
     */
    Instances(String inDiagnose, double newDistance){

        diagnose = inDiagnose;
        distance = newDistance;
    }

    /**
     * @return diagnose
     */
    String getDiagnose() {
        return diagnose;
    }

    /**
     * @return distance
     */
    double getDistance() {
        return distance;
    }
    /**
     * @return Object string representation
     */
    @Override
    public String toString() {
        return "Helper{" +
                "diagnose='" + diagnose + '\'' +
                ", distance=" + distance +
                '}';
    }
}
