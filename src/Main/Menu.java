package Main;

import data.Data;
import kNN.kNN_classifier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

/**
 * This is the main class, from where program will run.
 * Program uses openCSV-3.8.jar library to read/write csv files.
 * Please download it from  <a href="https://sourceforge.net/projects/opencsv/">https://sourceforge.net/projects/opencsv/</a>
 * and add it to the project dependencies.
 */
public class Menu {
    private static final String TITLE = "\n Data Mining coursework  \n" + " by Vidmantas Naravas\n\n"
            + "\t********************\n" + "\t1. use kNN to classify data \n" + "\t0. Exit \n"
            + "\t********************\n" + "Please input a single digit (0-1):\n";

    /**
     * Constructor for the menu class. Simple user interface.
     */
    private Menu() {
        int selected = -1;
        while (selected != 0) {
            System.out.println(TITLE);
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            try {
                selected = Integer.parseInt(in.readLine());
                switch (selected) {
                    case 1:
                        menu1();
                        break;
                }
            } catch (Exception ignored) {
            }
        }
        System.out.println("Bye!");
    }

    /**
     * Main method to run program.
     *
     * @param args main method takes array of arguments
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        new Menu();


    }


    /**
     * Here is little command line user interface.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    private void menu1() throws IOException, InterruptedException {
        //initialize data
        Data data = new Data(1000, 11);
        data.addData("breast_cancer_train.csv", "train");
        data.addData("breast_cancer_test.csv", "test");
        // String for scanner
        String choose = "Please choose any positive integer for k neighbours. " +
                "Or 0 to go back to the main menu.";
        String dot = ".";
        // variable for pause program
        int wait = 0;
        // colours of the output in the console
        final String RESET = "\u001B[0m";
        final String RED = "\u001B[31m";
        // get value from the from the user
        int selected = -1;
        while (selected != 0 || selected < 0) {
            System.out.println(choose);
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            try {
                //initialize kNN classifier
                kNN_classifier kNN = new kNN_classifier(data);
                selected = Integer.parseInt(in.readLine());
                while (wait != 4 && selected != 0) {
                    TimeUnit.MILLISECONDS.sleep(800);
                    if (wait < 1) {
                        System.out.print(RED + "Preparing data." + RESET);
                    }
                    System.out.print(RED + dot + RESET);
                    wait++;
                }
                System.out.println();
                kNN.kNN(selected);
                wait = 0;
            } catch (Exception ignored) {
            }
        }
        while (wait != 3) {
            TimeUnit.MILLISECONDS.sleep(500);
            if (wait < 1) {
                System.out.print(RED + "Going back to the main menu." + RESET);
            }
            System.out.print(RED + dot + RESET);
            wait++;
        }
        System.out.println();
    }
}